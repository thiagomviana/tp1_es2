/**
 * generated by Xtext
 */
package org.xtext.example.mydsl.ui.contentassist;

import org.xtext.example.mydsl.ui.contentassist.AbstractFowlerDslProposalProvider;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class FowlerDslProposalProvider extends AbstractFowlerDslProposalProvider {
}
