/*
 * generated by Xtext
 */
package org.xtext.example.mydsl.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import org.xtext.example.mydsl.ui.internal.FowlerDslActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class FowlerDslExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return FowlerDslActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return FowlerDslActivator.getInstance().getInjector(FowlerDslActivator.ORG_XTEXT_EXAMPLE_MYDSL_FOWLERDSL);
	}
	
}
