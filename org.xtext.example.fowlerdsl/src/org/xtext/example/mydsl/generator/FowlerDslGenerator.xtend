package org.xtext.example.mydsl.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import org.xtext.example.mydsl.fowlerDsl.Statemachine
import org.xtext.example.mydsl.fowlerDsl.Command
import org.xtext.example.mydsl.fowlerDsl.State

class FowlerDslGenerator implements IGenerator {

	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
		fsa.generateFile("../src/" + resource.className + ".java", toJavaCode(resource.contents.head as Statemachine))
	}

	def className(Resource res) {
		var name = res.URI.lastSegment
		name.substring(0, name.indexOf('.'))
	}

	def toJavaCode(Statemachine sm) '''
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
 
 public class «sm.eResource.className» {
  private FileWriter buff;
  private File in; 
  private Scanner inReader;
  private boolean EOFReached;

	public «sm.eResource.className»(String path) {
		EOFReached = false;
		
		try {
			buff = new FileWriter(new File("DslLog.txt"));
			in = new File(path);
			inReader = new Scanner(in);
		} catch (IOException e) {
			System.out.println("Erro na criação do arquivo de log ou leitura da entrada.");
		}
	}

	public static void main(String[] args) {
		new «sm.eResource.className»(args[0]).run();
	}
  
  «FOR c : sm.commands»
   «c.declareCommand»
  «ENDFOR»
  
  protected void run() {
   boolean executeActions = true;
   String currentState = "«sm.states.head.name»";
   String lastEvent = null;
   while (!EOFReached) {
    «FOR state : sm.states»
     «state.generateCode»
    «ENDFOR»
    «FOR resetEvent : sm.resetEvents»
     if ("«resetEvent.name»".equals(lastEvent)) {      
      try {
     buff.write("Resetando a máquina de estados.\n");
} catch (IOException e) {
     System.out.println("Erro na escrita do Log.");
}
      currentState = "«sm.states.head.name»";
      executeActions = true;
     }
    «ENDFOR»
    
   }
  }
  
 private String receiveEvent() {
	 if(inReader.hasNextLine())
		 return inReader.nextLine();
	 
	 EOFReached = true;
	 inReader.close();
	 try {
		buff.close();
	} catch (IOException e) {
		System.out.println("Erro ao fechar o arquivo de log");
	}
	 return "";
	}
 }
 '''

	def declareCommand(Command command) '''
		protected void do«command.name.toFirstUpper»() {
			try {
		buff.write("Executando comando «command.name» («command.code»)\n");
		} catch (IOException e) {
		System.out.println("Erro na escrita do Log.");
		}
		 
		}
	'''

	def generateCode(State state) '''
		if (currentState.equals("«state.name»")) {
		if (executeActions) {
		   «FOR c : state.actions»
		   	do«c.name.toFirstUpper»();
		   «ENDFOR»
		executeActions = false;
		}
		try {
		buff.write("Você agora está no estado '«state.name»'. Os eventos possíveis são [«state.transitions.map(t|t.event.name).
			join(', ')»].\n");
		} catch (IOException e) {
		System.out.println("Erro na escrita do Log.");
		}   
		lastEvent = receiveEvent();
		  «FOR t : state.transitions»
		  	if ("«t.event.name»".equals(lastEvent)) {
		  	currentState = "«t.state.name»";
		  	executeActions = true;
		  	}
		  «ENDFOR»
		}
	'''

}
